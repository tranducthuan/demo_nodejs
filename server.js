import express from 'express'
import * as path from "path";
import { fileURLToPath } from 'url';
import cookieParser from 'cookie-parser'
import { Routes } from "./src/index.routing.js";

const app = express();

// view engine setup
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.set('views', path.join(__dirname, 'src/views'));
app.set('view engine', 'jade');

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

Routes.v1(app)

try{
    app.listen(3000, async()=> {
    })
}catch(err){
    console.log(`Error on start server : ${err}`)
}

