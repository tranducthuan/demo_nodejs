import express from "express";
import { userController } from "../modules/controllers/user.controllers.js";

const router = express.Router();

/* GET users listing. */
router.get('/', userController.getAllUser);
router.post('/create', userController.createUser);
router.delete('/',userController.deleteUser);
router.get('/find-by-id', userController.findUserById);
router.post('/', userController.editUser);

export const usersRouter = router;
