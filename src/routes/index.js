import { usersRouter } from './users.js'
import { roleRoute } from "./role.js";

export const indexRoute = [
    usersRouter,
    roleRoute
]
