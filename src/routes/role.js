import express from "express";
import { roleController } from "../modules/controllers/role.controllers.js";

const router = express.Router();

router.get('/role', roleController.getAllRole)

export const roleRoute = router;
