import * as dotenv from 'dotenv'
dotenv.config()

export default {
    database : process.env.DATABASE_NAME,
    user : process.env.DATABASE_USER,
    password : process.env.DATABASE_PASSWORD,
    host_dialect:{
        host : process.env.DATABASE_HOST,
        dialect: process.env.DIALECT
    }
}
