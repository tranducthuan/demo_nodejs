import sequelize from 'sequelize';
import ENV from '../config/enviroment.js';

const { Sequelize, DataTypes } = sequelize;
const connectDB = new Sequelize(
    'demo_nodejs',
    'root',
    'secret',
    {
        host: '172.18.0.2',
        dialect: 'mysql',
        logging: false
    }
);

connectDB.sync({force: false, alter: true}).then(() => {
}).catch((error) => {
    console.error('Connect DB error : ', error);
});

export default connectDB;
