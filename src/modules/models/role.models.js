import conn from '../../config/database.js';
import DataTypes from 'sequelize'
const queryInterface = conn.getQueryInterface();

const Role = conn.define("roles", {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    name1: {
        type: DataTypes.STRING
    },
},{
    paranoid: true
});


export default Role;
