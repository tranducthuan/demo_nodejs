import User from '../models/user.models.js'
import sequelize from '../../config/database.js';
import QueryTypes from 'sequelize'

const queryTypes = QueryTypes

const getAllUser = async (res, req) => {
    let [row] = await sequelize.query('SELECT * FROM users',
        {
            type: queryTypes.SELECT
            }
        )
    if(row){
        console.log("row", row)
    }else{
        return Promise.reject('CONSTANTS.ERROR_RESPONSE.INVALID_TOKEN')
    }

}

const createUser = async (req, res) => {
    // try {
    //     await sequelize.transaction(async transaction => { // Note that we pass a callback rather than awaiting the call with no arguments
    //         const user = await User.findOne({
    //             where: {
    //                 id: req.body.id
    //             }
    //         }, { transaction });
    //         await user.update({
    //             title: "222 11test"
    //         }, { transaction })
    //     });
    //     // Committed
    // } catch(err) {
    //     // Rolled back
    //     console.error(err);
    // }
    sequelize.sync().then(() => {
        User.create({
            title: "test",
            author: "thuan",
            release_date: "2021-12-14",
            subject: 4
        }).then(res => {
            console.log('Create successfully')
        }).catch(error => {
            console.log(error)
        })
    })
}

const deleteUser = async (req, res) => {
    sequelize.sync().then(() => {
        User.destroy({
            where: {
                id: req.body.id
            }
        }).then(() => {
            console.log('Successfully deleted record.')
        }).catch(error => {
            console.log(error)
        })
    })
}

const findUserById = async (req, res) => {
    sequelize.sync().then(() => {
        User.findOne({
            where: {
                id: req.body.id
            }
        }).then(res => {
            console.log(res)
        }).catch(error => {
            console.log(error)
        })
    })
}

const editUser = async (req, res) => {
    sequelize.sync().then(() => {
        User.update({
            title: "thuan test"
        }, {
            where: {
                id: req.body.id
            }
        })
    })
}

export const userController = {
    getAllUser,
    createUser,
    deleteUser,
    findUserById,
    editUser
}
