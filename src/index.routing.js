'use strict';
import {indexRoute} from './routes/index.js'
/**
 * @author Node Developer
 * @description This file is responsible for to initialize all routing
 */
class Routig {
    /**
     * @param {Object} server server object which is responsible for the handle request
     *
     * @description This function is used to configure routing for a version 1
     */
    v1(server) {
        server.use("/api/v1",indexRoute);
    }
}

export const Routes = new Routig();
